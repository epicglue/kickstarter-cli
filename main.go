package main

import "gitlab.com/epicglue/kickstarter-cli/cmd"

func main() {
	cmd.Execute()
}
